# Landscape Generator

This is a voxel-based procedural landscape generator developed in Unity as part of my Bachelor project in Software Engineering at Heilbronn University. The project demonstrates a proof-of-concept for creating organic, dynamic terrains using procedural generation techniques.
You can download the latest release for this or my other projects from my portfolio page: [tionard.itch.io](https://tionard.itch.io) 
## Overview

Landscape Generator uses a voxel-based approach to build detailed terrains by combining:
- **Multi-layer Noise Generation:** Utilizes both Perlin and OpenSimplex noise to create varied and natural heightmaps.
- **Marching Cubes Algorithm:** A custom CPU-based implementation generates smooth terrain meshes from voxel data.
- **Advanced Terrain Features:** Supports cave generation and interactive terraforming, allowing real-time terrain modifications.

## Features

- **Procedural Terrain Creation:** Generate diverse landscapes with adjustable noise parameters for realistic variations.
- **Voxel-Based Mesh Generation:** Efficiently construct terrain using a voxel grid combined with the Marching Cubes algorithm.
- **Dynamic Terrain Modification:** Use terraforming tools to add, remove, or flatten terrain, enabling interactive environment editing.
- **Optimized Multi-threading:** Balances terrain generation speed and performance by distributing work across multiple CPU threads.

## Technologies

- **Unity:** Developed using Unity (version 2020.3.7f1) with the Universal Render Pipeline.
- **C#:** All core systems�including noise generation, mesh construction, and terraforming�are implemented in C#.