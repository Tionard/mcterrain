using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System.Collections.Generic;

public class UIManager : MonoBehaviour
{
    [Header("World Generator")]
    [SerializeField] private WorldGenerator wG;

    [Header("Panels")]
    [SerializeField] private GameObject terPanel;
    [SerializeField] private GameObject matPanel;

    [Header("Amplitude Sliders")]
    [SerializeField] private Slider ampSlider1;
    [SerializeField] private Slider ampSlider2;
    [SerializeField] private Slider ampSlider3;

    [Header("Persistance Sliders")]
    [SerializeField] private Slider perSlider1;
    [SerializeField] private Slider perSlider2;
    [SerializeField] private Slider perSlider3;

    [Header("Lacunarity Sliders")]
    [SerializeField] private Slider lacSlider1;
    [SerializeField] private Slider lacSlider2;
    [SerializeField] private Slider lacSlider3;

    [Header("Scale Sliders")]
    [SerializeField] private Slider scaSlider1;
    [SerializeField] private Slider scaSlider2;
    [SerializeField] private Slider scaSlider3;

    [Header("Material Sliders")]
    [SerializeField] private Slider matTopSlider;
    [SerializeField] private Slider matSideSlider;
    [SerializeField] private Slider matSharpSlider;
    [SerializeField] private Slider matSmoothSlider;
    [SerializeField] private Slider grassTSlider;
    [SerializeField] private Slider grassHBSlider;
    [SerializeField] private Slider grassAngleSlider;

    [SerializeField] private Slider watNTSlider;
    [SerializeField] private Slider watNSSlider;
    [SerializeField] private Slider watSmoothSlider;
    [SerializeField] private Slider watDepthSlider;
    [SerializeField] private Slider watDSSlider;
    [SerializeField] private Slider watSpd1Slider;
    [SerializeField] private Slider watSpd2Slider;

    [Header("Toggle Water and Caves")]
    [SerializeField] private Toggle watToggle;
    [SerializeField] private Toggle cavToggle;

    [Header("Noise Display")]
    [SerializeField] private GameObject mapPreviewPanel;
    [SerializeField] private NoiseDisplay displayController;

    [Header("UI Panels")]
    [SerializeField] private GameObject customizationPanel;
    [SerializeField] private GameObject selectMapPanel;

    [Header("Seed")]
    [SerializeField] private TMP_InputField seedInput;

    [Header("Buttons")]
    [SerializeField] private Button genButton;

    private GameMode previousGameMode = GameMode.FreeCam;
    private void OnEnable()
    {
        previousGameMode = GameData.gameMode;
        GameData.gameMode = GameMode.Editor;
        LockControls();
        UpdateUI();

        MuteCalls(watToggle.onValueChanged);
        MuteCalls(cavToggle.onValueChanged);
        watToggle.isOn = GameData.GenerateWater;
        cavToggle.isOn = GameData.GenerateCaves;
        UnmuteCalls(watToggle.onValueChanged);
        UnmuteCalls(cavToggle.onValueChanged);
    }

    private void OnDisable()
    {
        GameData.gameMode = previousGameMode;
    }

    public void UpdateUI()
    {
        MuteAllSlider();
        ampSlider1.value = GameData.TerrainAmplitude1;
        ampSlider2.value = GameData.TerrainAmplitude2;
        ampSlider3.value = GameData.TerrainAmplitude3;
        perSlider1.value = GameData.TerrainPersistance1;
        perSlider2.value = GameData.TerrainPersistance2;
        perSlider3.value = GameData.TerrainPersistance3;
        lacSlider1.value = GameData.TerrainLacunarity1;
        lacSlider2.value = GameData.TerrainLacunarity2;
        lacSlider3.value = GameData.TerrainLacunarity3;
        scaSlider1.value = GameData.TerrainScale1;
        scaSlider2.value = GameData.TerrainScale2;
        scaSlider3.value = GameData.TerrainScale3;
        matTopSlider.value = GameData.terrainMat.GetFloat("_MainTilingTop");
        matSideSlider.value = GameData.terrainMat.GetFloat("_MainTilingSide");
        matSharpSlider.value = GameData.terrainMat.GetFloat("_BlendSharpness");
        matSmoothSlider.value = GameData.terrainMat.GetFloat("_SmoothnessMain");
        grassTSlider.value = GameData.terrainMat.GetFloat("_GrassTiling");
        grassHBSlider.value = GameData.terrainMat.GetFloat("_GrassHeightBlend");
        grassAngleSlider.value = GameData.terrainMat.GetFloat("_GrassAngle");

        watNTSlider.value = GameData.waterMat.GetFloat("_NormalTiling");
        watNSSlider.value = GameData.waterMat.GetFloat("_NormalStrength");
        watSmoothSlider.value = GameData.waterMat.GetFloat("_SmoothnessMain");
        watDepthSlider.value = GameData.waterMat.GetFloat("_Depth");
        watDSSlider.value = GameData.waterMat.GetFloat("_Strength");
        watSpd1Slider.value = GameData.waterMat.GetFloat("_WaveSpeed1");
        watSpd2Slider.value = GameData.waterMat.GetFloat("_WaveSpeed2");
        UnmuteAllSlider();
    }

    private void Start()
    {
        UpdateNoiseDisplay();
        seedInput.text = GameData.seed.ToString();
    }

    public void SetValuesFromSliders()
    {
        //MuteAllSlider();
        GameData.TerrainAmplitude1 = ampSlider1.value;
        GameData.TerrainAmplitude2 = ampSlider2.value;
        GameData.TerrainAmplitude3 = ampSlider3.value;
        GameData.TerrainPersistance1 = perSlider1.value;
        GameData.TerrainPersistance2 = perSlider2.value;
        GameData.TerrainPersistance3 = perSlider3.value;
        GameData.TerrainLacunarity1 = lacSlider1.value;
        GameData.TerrainLacunarity2 = lacSlider2.value;
        GameData.TerrainLacunarity3 = lacSlider3.value;
        GameData.TerrainScale1 = scaSlider1.value;
        GameData.TerrainScale2 = scaSlider2.value;
        GameData.TerrainScale3 = scaSlider3.value;
        //UnmuteAllSlider();
        UpdateNoiseDisplay();
    }

    public void UpdateNoiseDisplay()
    {
        if (mapPreviewPanel.activeSelf)
        {
            int mapSize = GameData.WorldSizeInChunks * GameData.ChunkWidth;
            float[,] noiseMap = new float[mapSize, mapSize];
            for (int x = 0; x < mapSize; x++)
            {
                for (int z = 0; z < mapSize; z++)
                {
                    noiseMap[x, z] = GameData.GenerateTerrainHeightNoise(x, z) / GameData.ChunkHeight;
                }
            }

            displayController.DrawNoiseMap(noiseMap);
        }
    }

    public void SetNewSeed()
    {
        GameData.seed = int.Parse(seedInput.text);
        UpdateNoiseDisplay();
    }

    private void MuteAllSlider()
    {
        MuteCalls(ampSlider1.onValueChanged);
        MuteCalls(ampSlider2.onValueChanged);
        MuteCalls(ampSlider3.onValueChanged);
        MuteCalls(perSlider1.onValueChanged);
        MuteCalls(perSlider2.onValueChanged);
        MuteCalls(perSlider3.onValueChanged);
        MuteCalls(lacSlider1.onValueChanged);
        MuteCalls(lacSlider2.onValueChanged);
        MuteCalls(lacSlider3.onValueChanged);
        MuteCalls(scaSlider1.onValueChanged);
        MuteCalls(scaSlider2.onValueChanged);
        MuteCalls(scaSlider3.onValueChanged);

        MuteCalls(grassAngleSlider.onValueChanged);
        MuteCalls(grassHBSlider.onValueChanged);
        MuteCalls(grassTSlider.onValueChanged);
        MuteCalls(matSmoothSlider.onValueChanged);
        MuteCalls(matSideSlider.onValueChanged);
        MuteCalls(matTopSlider.onValueChanged);
        MuteCalls(matSharpSlider.onValueChanged);
        MuteCalls(watNTSlider.onValueChanged);
        MuteCalls(watNSSlider.onValueChanged);
        MuteCalls(watSmoothSlider.onValueChanged);
        MuteCalls(watDepthSlider.onValueChanged);
        MuteCalls(watDSSlider.onValueChanged);
        MuteCalls(watSpd1Slider.onValueChanged);
        MuteCalls(watSpd2Slider.onValueChanged);
    }

    private void UnmuteAllSlider()
    {
        UnmuteCalls(ampSlider1.onValueChanged);
        UnmuteCalls(ampSlider2.onValueChanged);
        UnmuteCalls(ampSlider3.onValueChanged);
        UnmuteCalls(perSlider1.onValueChanged);
        UnmuteCalls(perSlider2.onValueChanged);
        UnmuteCalls(perSlider3.onValueChanged);
        UnmuteCalls(lacSlider1.onValueChanged);
        UnmuteCalls(lacSlider2.onValueChanged);
        UnmuteCalls(lacSlider3.onValueChanged);
        UnmuteCalls(scaSlider1.onValueChanged);
        UnmuteCalls(scaSlider2.onValueChanged);
        UnmuteCalls(scaSlider3.onValueChanged);
        UnmuteCalls(grassAngleSlider.onValueChanged);
        UnmuteCalls(grassHBSlider.onValueChanged);
        UnmuteCalls(grassTSlider.onValueChanged);
        UnmuteCalls(matSmoothSlider.onValueChanged);
        UnmuteCalls(matSideSlider.onValueChanged);
        UnmuteCalls(matTopSlider.onValueChanged);
        UnmuteCalls(matSharpSlider.onValueChanged);
        UnmuteCalls(watNTSlider.onValueChanged);
        UnmuteCalls(watNSSlider.onValueChanged);
        UnmuteCalls(watSmoothSlider.onValueChanged);
        UnmuteCalls(watDepthSlider.onValueChanged);
        UnmuteCalls(watDSSlider.onValueChanged);
        UnmuteCalls(watSpd1Slider.onValueChanged);
        UnmuteCalls(watSpd2Slider.onValueChanged);
    }


    private void MuteCalls(UnityEngine.Events.UnityEventBase ev)
    {
        int count = ev.GetPersistentEventCount();
        for (int i = 0; i < count; i++)
        {
            ev.SetPersistentListenerState(i, UnityEngine.Events.UnityEventCallState.Off);
        }
    }

    private void UnmuteCalls(UnityEngine.Events.UnityEventBase ev)
    {
        int count = ev.GetPersistentEventCount();
        for (int i = 0; i < count; i++)
        {
            ev.SetPersistentListenerState(i, UnityEngine.Events.UnityEventCallState.RuntimeOnly);
        }
    }

    public void RegenerateMesh() => wG.ReconstructMesh();

    public void LockControls()
    {
        watToggle.interactable = false;
        cavToggle.interactable = false;
        ampSlider1.interactable = false;
        ampSlider2.interactable = false;
        ampSlider3.interactable = false;
        perSlider1.interactable = false;
        perSlider2.interactable = false;
        perSlider3.interactable = false;
        lacSlider1.interactable = false;
        lacSlider2.interactable = false; 
        lacSlider3.interactable = false;
        scaSlider1.interactable = false;
        scaSlider2.interactable = false;
        scaSlider3.interactable = false;
        seedInput.interactable = false;
        genButton.interactable = false;
    }

    public void UnlockControls()
    {
        watToggle.interactable = true;
        cavToggle.interactable = true;
        ampSlider1.interactable = true;
        ampSlider2.interactable = true;
        ampSlider3.interactable = true;
        perSlider1.interactable = true;
        perSlider2.interactable = true;
        perSlider3.interactable = true;
        lacSlider1.interactable = true;
        lacSlider2.interactable = true;
        lacSlider3.interactable = true;
        scaSlider1.interactable = true;
        scaSlider2.interactable = true;
        scaSlider3.interactable = true;
        seedInput.interactable = true;
        genButton.interactable = true;
    }

    private void Update()
    {

        if (wG.chunksInQueue.Count < 1 && genButton.interactable == false)
        {
            UnlockControls();
        }

        if (wG.chunksInQueue.Count > 0 && genButton.interactable == true)
        {
            LockControls();
        }
   
    }

    public void ToggleWaterGeneration()
    {
        GameData.GenerateWater = !GameData.GenerateWater;
        //watToggle.isOn = GameData.GenerateWater;
    }
    public void ToggleCavesGeneration()
    {
        GameData.GenerateCaves = !GameData.GenerateCaves;
        //cavToggle.isOn = GameData.GenerateCaves;
    }

    public void ToggleMapPanel()
    {
        mapPreviewPanel.SetActive(!mapPreviewPanel.activeSelf);
        UpdateNoiseDisplay();
    }

    public void SetWorldSize(int size)
    {
        GameData.WorldSizeInChunks = size;
        customizationPanel.SetActive(true);
        selectMapPanel.SetActive(false);
        ToggleMapPanel();
    }

    public void SetMatTopTiling()
    {
        GameData.terrainMat.SetFloat("_MainTilingTop", matTopSlider.value);
    }
    public void SetMatSideTiling()
    {
        GameData.terrainMat.SetFloat("_MainTilingSide", matSideSlider.value);
    }

    public void SetMatBlendSharpness()
    {
        GameData.terrainMat.SetFloat("_BlendSharpness", matSharpSlider.value);
    }

    public void SetMatSmoothness()
    {
        GameData.terrainMat.SetFloat("_SmoothnessMain", matSmoothSlider.value);
    }
    public void SetGrassTiling()
    {
        GameData.terrainMat.SetFloat("_GrassTiling", grassTSlider.value);
    }
    public void SetGrassHeightBlend()
    {
        GameData.terrainMat.SetFloat("_GrassHeightBlend", grassHBSlider.value);
    }
    public void SetGrassAngleBlend()
    {
        GameData.terrainMat.SetFloat("_GrassAngle", grassAngleSlider.value);
    }

    public void SetWaterNormalTiling()
    {
        GameData.waterMat.SetFloat("_NormalTiling", watNTSlider.value);
    }

    public void SetWaterNormalStrength()
    {
        GameData.waterMat.SetFloat("_NormalStrength", watNSSlider.value);
    }
    public void SetWaterDepth()
    {
        GameData.waterMat.SetFloat("_Depth", watDepthSlider.value);
    }

    public void SetWaterDepthStrenght()
    {
        GameData.waterMat.SetFloat("_Strength", watDSSlider.value);
    }

    public void SetWaterSmoothness()
    {
        GameData.waterMat.SetFloat("_SmoothnessMain", watSmoothSlider.value);
    }

    public void SetWaterSpeed1()
    {
        GameData.waterMat.SetFloat("_WaveSpeed1", watSpd1Slider.value);
    }

    public void SetWaterSpeed2()
    {
        GameData.waterMat.SetFloat("_WaveSpeed2", watSpd2Slider.value);
    }


    public void MaterialPanelOn()
    {
        terPanel.SetActive(false);
        matPanel.SetActive(true);     
    }

    public void TerrainPanelOn()
    {
        matPanel.SetActive(false);
        terPanel.SetActive(true);
    }
}
