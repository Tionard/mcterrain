using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public Camera mainCam;
    bool flyingMode = false;
    [SerializeField]
    private WorldGenerator world = null;

    [SerializeField] private GameObject uiPanelLeft;
    [SerializeField] private GameObject visualizerVoxel;
    [SerializeField] private GameObject visualizerTerraform;
    [SerializeField] private GameObject visualizerFlattener;
    [SerializeField] private LayerMask terrainLayer;
    private GameObject visualizer;
    private bool terraformingMode;

    public float camSpeed = 10f;

    [Header("Terraforming")]
    [SerializeField] [Range(1.25f, 10f)] private float radius = 3f;
    [SerializeField] [Range(1.25f, 10f)] private float deformationSpeed = 5f;
    private float deformCooldownTimer = -0.1f;


    private void Start()
    {
        visualizer = visualizerTerraform;
        visualizerVoxel.SetActive(false);
        uiPanelLeft.SetActive(false);
    }
    
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Tab) && GameData.gameMode != GameMode.Editor)
        {
            terraformingMode = !terraformingMode;
            if (terraformingMode)
            {
                GameData.gameMode = GameMode.Terraforming;               
            }
            else
            {
                GameData.gameMode = GameMode.FreeCam;
            }
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            flyingMode = !flyingMode;
            if (flyingMode)
            {
                Cursor.lockState = CursorLockMode.Locked;
                uiPanelLeft.SetActive(false);
            }
            else
            {
                Cursor.lockState = CursorLockMode.Confined;
                Cursor.lockState = CursorLockMode.None;
                uiPanelLeft.SetActive(true);
            }
        }

        if (Input.GetKeyDown(KeyCode.Alpha1) && Input.GetKey(KeyCode.C))
        {
            camSpeed = 10f;
        }
        else if (Input.GetKeyDown(KeyCode.Alpha2) && Input.GetKey(KeyCode.C))
        {
            camSpeed = 40f;
        }
        else if (Input.GetKeyDown(KeyCode.Alpha3) && Input.GetKey(KeyCode.C))
        {
            camSpeed = 80f;
        }

        if (Input.GetKeyDown(KeyCode.Alpha1) && Input.GetKey(KeyCode.V))
        {
            visualizer = visualizerTerraform;
            visualizerVoxel.SetActive(false);
            visualizerTerraform.SetActive(true);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha2) && Input.GetKey(KeyCode.V))
        {
            visualizer = visualizerVoxel;
            visualizerTerraform.SetActive(false);
            visualizerVoxel.SetActive(true);
        }

        if (visualizerTerraform.activeSelf)
        {
            visualizer.transform.localScale = new Vector3(radius * 2f, radius * 2f, radius * 2f);
        }

        ChangeTerraformingRadius();

        if (GameData.gameMode != GameMode.Editor)
        {
            transform.position = Vector3.MoveTowards(transform.position, transform.position +
                    (mainCam.transform.forward * Input.GetAxis("Vertical")) +
                    (transform.right * Input.GetAxis("Horizontal")), Time.deltaTime * camSpeed);

            transform.Rotate(new Vector3(0, Input.GetAxis("Mouse X"), 0));
            mainCam.transform.Rotate(new Vector3(-Input.GetAxis("Mouse Y"), 0, 0));

            if (terraformingMode)
            {
                Ray ray = mainCam.ViewportPointToRay(new Vector3(0.5f, 0.5f, 1f));
                RaycastHit hit;


                if (Input.GetKey(KeyCode.LeftShift))
                {
                    visualizerFlattener.SetActive(true);
                    visualizer.SetActive(false);
                    
                }
                else
                {
                    visualizerFlattener.SetActive(false);
                    visualizer.SetActive(true);
                }

                if (Physics.Raycast(ray, out hit, 200, terrainLayer))
                {

                    if (visualizerVoxel.activeSelf)
                    {
                        visualizer.transform.position = new Vector3(0.5f + (int)hit.point.x, 0.5f + (int)hit.point.y, 0.5f + (int)hit.point.z);
                    }
                    if (visualizerTerraform.activeSelf)
                    {
                        visualizer.transform.position = hit.point;
                    }
                    if (visualizerFlattener.activeSelf)
                    {
                        visualizerFlattener.transform.position = new Vector3(0.5f + (int)hit.point.x, 0.5f + (int)hit.point.y, 0.5f + (int)hit.point.z);
                        visualizerFlattener.transform.localScale = new Vector3(radius * 2f, 0.25f, radius * 2f);
                    }

                    deformCooldownTimer = Mathf.Max(-0.1f, deformCooldownTimer - Time.deltaTime);

                    Debug.Log("0: "+deformationSpeed);
                    if (Input.GetMouseButton(1) && deformCooldownTimer <= 0 && !Input.GetKey(KeyCode.LeftShift)) 
                    {
                        deformCooldownTimer = (1 / deformationSpeed);
                        world.ModifyTerrain(hit.point, radius, TerraformingDeformationType.Removing);
                    }
                    if (Input.GetMouseButton(0) && deformCooldownTimer <= 0)
                    {
                        deformCooldownTimer = (1 / deformationSpeed);
                        if (Input.GetKey(KeyCode.LeftShift))
                        {
                            world.ModifyTerrain(hit.point, radius, TerraformingDeformationType.Flattening);
                        }
                        else
                        {
                            world.ModifyTerrain(hit.point, radius, TerraformingDeformationType.Adding);
                        }
                    }
                    Debug.Log("1: " + deformationSpeed);

                }
            }
            else if(visualizer != null && visualizer.activeSelf)
            {
                visualizer.SetActive(false);
            }
        }

    }

    void ChangeTerraformingRadius()
    {
        radius += Input.mouseScrollDelta.y * 0.25f;
        if (radius < 1)
            radius = 1;
        if (radius > 8)
            radius = 8;
    }

    /*
    // draw gizmo of a viewed voxel
    private void OnDrawGizmos()
    {
        if (Input.GetKey(KeyCode.LeftShift))
        {
            Ray ray = mainCam.ViewportPointToRay(new Vector3(0.5f, 0.5f, 1f));
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit))
            {
                if (hit.transform.tag == "Terrain")
                {
                    Handles.zTest = UnityEngine.Rendering.CompareFunction.Less;
                    Handles.DrawWireCube(new Vector3(0.5f + (int)hit.point.x, 0.5f + (int)hit.point.y, 0.5f + (int)hit.point.z), Vector3.one);
                }
            }
        }
    }
    */


}
