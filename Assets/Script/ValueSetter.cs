using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ValueSetter : MonoBehaviour
{
    [SerializeField] private TMP_Text valueLabel;
    [SerializeField] private Slider slider;

    private void OnEnable()
    {
        SetSlidersValue();
    }
    public void SetSlidersValue()
    {
        valueLabel.SetText("" + Mathf.Round(slider.value * 100) / 100);
    }
}
