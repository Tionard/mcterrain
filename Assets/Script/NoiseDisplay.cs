using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NoiseDisplay : MonoBehaviour
{
    public Renderer textureRenderer;

    static Color lightGreen = new Color(0.293f, 0.86f, 0.31f);
    static Color lightBlue = new Color(0.37f, 0.72f, 0.88f);
    Texture2D noiseTexture;
    public RawImage noiseDp;
    [SerializeField] private Gradient landGradient;

    public void DrawNoiseMap(float[,] noiseMap)
    {
        int width = noiseMap.GetLength(0);
        int height = noiseMap.GetLength(1);

        noiseTexture = new Texture2D(width, height);

        Color[] colorMap = new Color[width * height];

        for (int z = 0; z < height; z++)
        {
            for (int x = 0; x < width; x++)
            {
                colorMap[z * width + x] = Color.Lerp(Color.black, Color.white, noiseMap[x, z]);
                if (colorMap[z * width + x].grayscale < (GameData.BaseWaterLevel / GameData.ChunkHeight))
                {
                    if (GameData.GenerateWater)
                    {   
                        //Make color brighter for underwater
                        colorMap[z * width + x].r += 0.2f;
                        colorMap[z * width + x].g += 0.2f;
                        colorMap[z * width + x].b += 0.2f;
                        colorMap[z * width + x] *= lightBlue;
                    }
                    else
                    {
                        colorMap[z * width + x] *= landGradient.Evaluate(colorMap[z * width + x].grayscale - GameData.BaseWaterLevel / GameData.ChunkHeight);
                    }
                        
                }
                else
                {
                    colorMap[z * width + x] *= landGradient.Evaluate(colorMap[z * width + x].grayscale - GameData.BaseWaterLevel / GameData.ChunkHeight);
                }
            }
        }
        noiseTexture.SetPixels(colorMap);
        noiseTexture.Apply();
        noiseTexture.filterMode = FilterMode.Point;

        noiseDp.texture = noiseTexture;
        noiseDp.texture.filterMode = FilterMode.Point;

        textureRenderer.sharedMaterial.mainTexture = noiseTexture;
        textureRenderer.transform.localScale = new Vector3(width, 1, height);
    }
}
