using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Marching : MonoBehaviour
{

    [SerializeField] private bool smoothTerrain; //
    [SerializeField] private bool shareVertices; //flat-shaded effect

    List<Vector3> vertices = new List<Vector3>();
    List<int> triangles = new List<int>();

    MeshFilter meshFilter;
    MeshCollider meshCollider;

    float terrainSurface = 0.5f; // density value that defines the edge between air and solir
    int chunkSize = 32;
    float[,,] terrainMap;

    private void Start()
    {
        meshFilter = GetComponent<MeshFilter>();
        meshCollider = GetComponent<MeshCollider>();
        transform.tag = "Terrain";

        terrainMap = new float[chunkSize + 1, chunkSize + 1, chunkSize + 1];

        PopulateTerrainMap();
        //CreateMeshData();
        BuildMesh();
    }


    void PopulateTerrainMap()
    {
        for (int x = 0; x < chunkSize + 1; x++)
        {
            for (int y = 0; y < chunkSize + 1; y++)
            {
                for (int z = 0; z < chunkSize + 1; z++)
                {
                    //calculating height of the edge between solid and air
                    float thisHeight = (float)8 * Mathf.PerlinNoise((float)x / 16f * 1.5f + 0.001f, (float)z / 16f * 1.5f + 0.001f);

                    //populating terrain mesh with reverse height values
                    terrainMap[x, y, z] = (float)y - thisHeight;
                    
                    //Optimized way to create mesh Data without extra iterration
                    //We want first to populate terrain and then MarchCubes on generated part of the terrain
                    //That's why MarchCube algorithm is called one step later. Usually it requires extra for-loop itteration, look CreateMeshData() function
                    if(x > 0 && y > 0 && z > 0)
                        MarchCube(new Vector3Int(x-1, y-1, z-1));
                    
                }
            }
        }
    }

    void CreateMeshData()
    {
        ClearMeshData();
        for (int x = 0; x < chunkSize; x++)
        {
            for (int y = 0; y < chunkSize; y++)
            {
                for (int z = 0; z < chunkSize; z++)
                {
                    MarchCube(new Vector3Int(x, y, z));
                }
            }
        }
        BuildMesh();
    }

    int GetCubeConfiguration(float[] cube)
    {
        int configIndex = 0;
        for (int i = 0; i < 8; i++)
        {
            if (cube[i] > terrainSurface)
                configIndex |= 1 << i;
        }

        return configIndex;
    }

    void MarchCube(Vector3Int position)
    {
        // Sample terrain values at each corner of the cube.
        float[] cube = new float[8];
        for (int i = 0; i < 8; i++)
        {
            cube[i] = SampleTerrain(position + GameData.CornerTable[i]);
        }

        // Get the configuration index of this cube.
        int configIndex = GetCubeConfiguration(cube);

        // If the configuration of the cube is 0 or 255 (hidden solid or air) we don't do anything.
        if (configIndex == 0 || configIndex == 255)
            return;

        // Loop trough triangles. There are never more than 5 triangles to a cube, 3 vertices to a triangle.
        int edgeIndex = 0;
        for (int triangle = 0; triangle < 5; triangle++)
        {
            for (int point = 0; point < 3; point++)
            {
                // Get the current indice. We increment triangleIndex each loop.
                int indice = GameData.TriangleTable[configIndex, edgeIndex];

                // If the current edge is -1, there are no more indices and we can exit the function.
                if (indice == -1)
                    return;
                
                // Get the vertices for the start and end of this edge.
                Vector3 vert1 = position + GameData.CornerTable[GameData.EdgeIndexes[indice, 0]];
                Vector3 vert2 = position + GameData.CornerTable[GameData.EdgeIndexes[indice, 1]];

                Vector3 vertPosition = Vector3.zero;
                if (smoothTerrain)
                {
                    // Get the terrain values at either end of our current edge from the cube array created above.
                    float vert1Sample = cube[GameData.EdgeIndexes[indice, 0]];
                    float vert2Sample = cube[GameData.EdgeIndexes[indice, 1]];

                    // Calculate the difference between the terrain values.
                    float difference = vert2Sample - vert1Sample;

                    // If the difference is 0, then the terrain is passes through the middle.
                    if (difference == 0)
                        difference = terrainSurface;
                    else
                        difference = (terrainSurface - vert1Sample) / difference;

                    // Calculate the point along the edge that the terrain passes trough.
                    vertPosition = vert1 + ((vert2 - vert1) * difference);
                }
                else
                {
                    // Get the midpoint of this edge.
                    vertPosition = (vert1 + vert2) / 2f;
                }

                // Add to our vertices and triangles list and increment the edgeIndex.
                if (shareVertices)
                {
                    triangles.Add( VertForIndice(vertPosition));
                }
                else
                {
                    vertices.Add(vertPosition);
                    triangles.Add(vertices.Count - 1);
                }

                edgeIndex++;
            }
        }
    }

    public void PlaceTerrain(Vector3 pos)
    {
        Vector3Int v3Int = new Vector3Int(Mathf.CeilToInt(pos.x), Mathf.CeilToInt(pos.y), Mathf.CeilToInt(pos.z));
        for(int x = Mathf.Max(v3Int.x - 2 , 0); x < Mathf.Min(v3Int.x + 2, chunkSize + 1); x++)
            for (int y = Mathf.Max(v3Int.y - 2, 1); y < Mathf.Min(v3Int.y + 2, chunkSize); y++)
                for (int z = Mathf.Max(v3Int.z - 2, 0); z < Mathf.Min(v3Int.z + 2, chunkSize + 1); z++)
                {
                    terrainMap[x, y, z] = 0f;              
                }
        //terrainMap[v3Int.x, v3Int.y, v3Int.z] = 0f;
        CreateMeshData();
    }

    public void RemoveTerrain(Vector3 pos)
    {
        Vector3Int v3Int = new Vector3Int(Mathf.CeilToInt(pos.x), Mathf.CeilToInt(pos.y), Mathf.CeilToInt(pos.z));
        for (int x = Mathf.Max(v3Int.x - 2, 0); x < Mathf.Min(v3Int.x + 2, chunkSize + 1); x++)
            for (int y = Mathf.Max(v3Int.y - 2, 1); y < Mathf.Min(v3Int.y + 2, chunkSize + 1); y++)
                for (int z = Mathf.Max(v3Int.z - 2, 0); z < Mathf.Min(v3Int.z + 2, chunkSize + 1); z++)
                {
                    terrainMap[x, y, z] = 1f;
                }
        //terrainMap[v3Int.x, v3Int.y, v3Int.z] = 1f;
        CreateMeshData();
    }

    //Get the terrain value storred at given point of the terrainMap.
    float SampleTerrain(Vector3Int point)
    {
        return terrainMap[point.x, point.y, point.z];
    }

    int VertForIndice(Vector3 vert)
    {
        // Loop through all the vertices currently in the vertices list.
        for (int i = 0; i < vertices.Count; i++)
        {
            // If we find a vert that matches ours, then simply return this index
            if (vertices[i] == vert)
                return i;
        }

        // If we didn't find a match, add this vert to the list and return last index.
        vertices.Add(vert);
        return vertices.Count - 1;
    }

    //Use it before you repopulate mesh data
    void ClearMeshData()
    {
        vertices.Clear();
        triangles.Clear();
    }

    void BuildMesh()
    {
        Mesh mesh = new Mesh();
        mesh.vertices = vertices.ToArray();
        mesh.triangles = triangles.ToArray();

        Vector2[] uvs1 = new Vector2[vertices.Count];
        Vector2[] uvs2 = new Vector2[vertices.Count];
        Vector2[] uvs3 = new Vector2[vertices.Count];

        for (int i = 0; i < uvs1.Length; i++)
        {
            uvs1[i] = new Vector2(vertices[i].x, vertices[i].z);
            uvs2[i] = new Vector2(vertices[i].x, vertices[i].y);
            uvs3[i] = new Vector2(vertices[i].y, vertices[i].z);
        }
        mesh.uv = uvs1;
        mesh.uv2 = uvs2;
        mesh.uv3 = uvs3;

        mesh.RecalculateNormals();
        meshFilter.mesh = mesh;
        meshCollider.sharedMesh = mesh;
    }

}




