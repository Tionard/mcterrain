using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class NoiseGenerator
{
    public static float[,] GenerateNoiseMap(int mapSize, int seed, float scale, int octaves, float persistance, float lacunarity, Vector2 offsetOctaves, Vector2 offset, bool rigid)
    {
        float[,] noiseMap = new float[mapSize, mapSize];

        System.Random prng = new System.Random(seed);
        Vector2[] octaveOffsets = new Vector2[octaves];

        for(int i = 0; i < octaves; i++)
        {
            float offsetX = prng.Next(-100000, 100000) + offsetOctaves.x;
            float offsetZ = prng.Next(-100000, 100000) + offsetOctaves.y;
            octaveOffsets[i] = new Vector2(offsetX, offsetZ);
        }

        if (scale <= 0)
        {
            scale = 0.0001f;
        }

        float maxNoiseHeight = float.MinValue;
        float minNoiseHeight = float.MaxValue;

        float halfMapSize = mapSize / 2f;

        for (int z = 0 + (int) offset.y; z < mapSize + (int)offset.y; z++)
        {
            for (int x = 0 + (int) offset.x; x < mapSize + (int)offset.x; x++)
            {
                float amplitude = 1.2f;
                float frequency = 1.1f;
                float noiseHeight = 0;
                float weight = 1;

                for (int i = 0; i < octaves; i++)
                {
                    float v;
                    float offsetX = prng.Next(-100000, 100000) + offsetOctaves.x;
                    float offsetZ = prng.Next(-100000, 100000) + offsetOctaves.y;

                    float sampleX = (x - halfMapSize) / scale * frequency + octaveOffsets[i].x;
                    float sampleZ = (z - halfMapSize) / scale * frequency + octaveOffsets[i].y;
                    float perlinValue = Mathf.PerlinNoise(sampleX, sampleZ) * 2 - 1;

                    if ( rigid )
                    {
                        v = 1 - Mathf.Abs(Mathf.PerlinNoise((x - halfMapSize) / scale * frequency + octaveOffsets[i].x, (z - halfMapSize) / scale * frequency + octaveOffsets[i].y));
                        v *= v;
                        v *= weight;
                        weight = Mathf.Clamp(v * 0.8f, 0, 1);
                        noiseHeight += v * amplitude;
                    }
                    else
                    {
                        v = Mathf.PerlinNoise((x - halfMapSize) / scale * frequency + octaveOffsets[i].x, (z - halfMapSize) / scale * frequency + octaveOffsets[i].y);
                        noiseHeight += (v + 1) * 0.5f * amplitude;
                    }

                    //noiseHeight += perlinValue * amplitude;

                    amplitude *= persistance;
                    frequency *= lacunarity;
                    
                }

                if (noiseHeight > maxNoiseHeight)
                {
                    maxNoiseHeight = noiseHeight;
                } else if (noiseHeight < minNoiseHeight)
                {
                    minNoiseHeight = noiseHeight;
                }

                noiseMap[x - (int) offset.x, z - (int) offset.y] = noiseHeight;
            }
        }

        for (int z = 0; z < mapSize; z++)
        {
            for (int x = 0; x < mapSize; x++)
            {
                noiseMap[x, z] = Mathf.InverseLerp(minNoiseHeight, maxNoiseHeight, noiseMap[x, z]);
            }
        }

        return noiseMap;
    }


    public static float EvaluatePN(int x, int z, int seed, float scale, int octaves, float persistence, float lacunarity, Vector2 offset, bool rigid)
    {
        System.Random prng = new System.Random(seed);

        if (scale <= 0)
        {
            scale = 0.0001f;
        }

        float amplitude = 1.2f;
        float frequency = 1f;
        float noiseHeight = 0;
        float weight = 1f;

        for (int i = 0; i < octaves; i++)
        {

            float offsetX = prng.Next(-100000, 100000) + offset.x;
            float offsetZ = prng.Next(-100000, 100000) + offset.y;

            float val;
            if (rigid && i < 3)
            {
                val = 1 - Mathf.Abs(Mathf.PerlinNoise(x / scale * frequency + offsetX, z / scale * frequency + offsetZ));
                val *= val;
                val *= weight;
                weight = Mathf.Clamp(val * 0.8f, 0, 1);
                noiseHeight += val * amplitude;
            }
            else
            {
                val = Mathf.PerlinNoise(x / scale * frequency + offsetX, z / scale * frequency + offsetZ);
                noiseHeight += (val + 1) * 0.5f * amplitude;
            }

            amplitude *= persistence;
            frequency *= lacunarity;
        }

        return noiseHeight;
    }

    public static float[,] EvaluateOSN(int mapSize, int seed, float scale, int octaves, float persistence, float lacunarity, Vector2 offsetOctaves, Vector2 offset, bool rigid)
    {
        float[,] noiseMap = new float[mapSize, mapSize];
        OpenSimplexNoise simplexNoise = new OpenSimplexNoise(seed);
        System.Random prng = new System.Random(seed);
        Vector2[] octaveOffsets = new Vector2[octaves];

        for (int i = 0; i < octaves; i++)
        {
            float offsetX = prng.Next(-100000, 100000) + offsetOctaves.x;
            float offsetZ = prng.Next(-100000, 100000) + offsetOctaves.y;
            octaveOffsets[i] = new Vector2(offsetX, offsetZ);
        }

        if (scale <= 0)
        {
            scale = 0.0001f;
        }

        float maxNoiseHeight = float.MinValue;
        float minNoiseHeight = float.MaxValue;

        float halfMapSize = mapSize / 2f;

        for (int z = 0 + (int)offset.y; z < mapSize + (int)offset.y; z++)
        {
            for (int x = 0 + (int)offset.x; x < mapSize + (int)offset.x; x++)
            {
                float amplitude = 1.2f;
                float frequency = 1.1f;
                float noiseHeight = 0;
                float weight = 1;

                for (int i = 0; i < octaves; i++)
                {
                    float v;
                    //float sampleX = (x - halfMapSize) / scale * frequency + octaveOffsets[i].x;
                    //float sampleZ = (z - halfMapSize) / scale * frequency + octaveOffsets[i].y;

                    float sampleX = (x - halfMapSize) / scale * frequency + octaveOffsets[i].x;
                    float sampleZ = (z - halfMapSize) / scale * frequency + octaveOffsets[i].y;
                    float perlinValue =(float) (1 + simplexNoise.Evaluate(sampleX, sampleZ))*0.5f * 2 - 1;

                    if (rigid && i == 0)
                    {
                        v = 1 - Mathf.Abs((float)(1 + simplexNoise.Evaluate((x - halfMapSize) / scale * frequency + octaveOffsets[i].x, (z - halfMapSize) / scale * frequency + octaveOffsets[i].y))*0.5f);
                        v *= v;
                        v *= weight;
                        weight = Mathf.Clamp(v * 0.8f, 0, 1);
                        noiseHeight += v * amplitude;
                    }
                    else
                    {
                        v = (float)(1 + simplexNoise.Evaluate((x - halfMapSize) / scale * frequency + octaveOffsets[i].x, (z - halfMapSize) / scale * frequency + octaveOffsets[i].y))*0.5f;
                        noiseHeight += (v + 1) * 0.5f * amplitude;
                    }

                    //noiseHeight += perlinValue * amplitude;

                    amplitude *= persistence;
                    frequency *= lacunarity;

                }

                if (noiseHeight > maxNoiseHeight)
                {
                    maxNoiseHeight = noiseHeight;
                }
                else if (noiseHeight < minNoiseHeight)
                {
                    minNoiseHeight = noiseHeight;
                }

                noiseMap[x - (int)offset.x, z - (int)offset.y] = noiseHeight;
            }
        }

        for (int z = 0; z < mapSize; z++)
        {
            for (int x = 0; x < mapSize; x++)
            {
                noiseMap[x, z] = Mathf.InverseLerp(minNoiseHeight, maxNoiseHeight, noiseMap[x, z]);
            }
        }

        return noiseMap;
    }


    public static float EvaluateOpenSimplex(int x, int y, int z, int seed, float scale, int octaves, float persistance, float lacunarity, Vector3 offset, bool rigid)
    {
        OpenSimplexNoise simplexNoise = new OpenSimplexNoise(seed);
        System.Random prng = new System.Random(seed);

        if (scale <= 0)
        {
            scale = 0.0001f;
        }

        float amplitude = 1.2f;
        float frequency = 1f;
        float noiseHeight = 0;
        float weight = 1f;

        for (int i = 0; i < octaves; i++)
        {
            float offsetX = prng.Next(-100000, 100000) + offset.x;
            float offsetZ = prng.Next(-100000, 100000) + offset.z;
            float offsetY = prng.Next(-100000, 100000) + offset.y;

            float v;
            if (rigid && i < 2)
            {
                v = (float) simplexNoise.Evaluate(x / scale * frequency + offsetX, y / scale * frequency + offsetY, z / scale * frequency + offsetZ);
                v *= v;
                v *= weight;
                weight = Mathf.Clamp(v * 0.8f, -1, 1);
                noiseHeight += v * amplitude;
            }
            else
            {
                v = (float)simplexNoise.Evaluate(x / scale * frequency + offsetX, y / scale * frequency + offsetY, z / scale * frequency + offsetZ);
                noiseHeight += v * 0.5f * amplitude;
            }

            amplitude *= persistance;
            frequency *= lacunarity;
        }

        return noiseHeight;
    }



    private static float[,] _map;

    private static bool _useRandomSeed;
    private static int _seed;

    private static int _mapSize;
    private static int _randomFeelPrecent;
    private static int _smoothingSteps;



    public static void InitializeCaveNoise(int mapSize, int seed, bool useRandomSeed, int randomFeelPrecent, int smoothingSteps)
    {
        _mapSize = mapSize;
        _seed = seed;
        _useRandomSeed = useRandomSeed;
        _randomFeelPrecent = randomFeelPrecent;
        _smoothingSteps = smoothingSteps;

    }


    public static float[,] RandomFillMap()
    {
        ///
        _map = new float[_mapSize, _mapSize];
        ///

        if (_useRandomSeed)
        {
            _seed = UnityEngine.Random.Range(0, 100000);
        }

        System.Random pseudoRandom = new System.Random(_seed);

        for (int x = 0; x < _mapSize; x++)
        {
            for (int z = 0; z < _mapSize; z++)
            {
                if (x == 0 || x == (_mapSize - 1) || z == 0 || z == (_mapSize - 1))
                {
                    _map[x, z] = 1;
                }
                else
                {
                    _map[x, z] = (pseudoRandom.Next(0, 100) < _randomFeelPrecent) ? 1 : 0;
                }
            }
        }

        for(int i = 0; i < _smoothingSteps; i++)
        {
            SmoothMap();
        }

        return _map;
    }

    private static void SmoothMap()
    {
        for (int x = 0; x < _mapSize; x++)
        {
            for (int z = 0; z < _mapSize; z++)
            {
                int neighbourWallTiles = GetSurroundingTiles(x, z);
                if(neighbourWallTiles > 4f)
                {

                    _map[x, z] = 1;

                }
                else if(neighbourWallTiles < 4f)
                {
                    _map[x, z] = 0;
                }
            }
        }
    }

    private static int GetSurroundingTiles(int gridX, int gridZ)
    {
        int wallCount = 0;
        for(int neighbourX = gridX -1; neighbourX <= gridX + 1; neighbourX++)
        {
            for (int neighbourZ = gridZ - 1; neighbourZ <= gridZ + 1; neighbourZ++)
            {   
                if(neighbourX >= 0 && neighbourX < _mapSize && neighbourZ >= 0 && neighbourZ < _mapSize)
                {
                    if (neighbourX != gridX || neighbourZ != gridZ)
                    {
                        wallCount += (int) _map[neighbourX, neighbourZ];
                    }
                }
                else
                {
                    wallCount++;
                }

            }
        }
        return wallCount;
    }
}
