using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapGenerator : MonoBehaviour
{
    [Header("Noise Type")]
    [SerializeField] private NoiseType noiseType = NoiseType.HEIGHTMAP;

    [Header("General Noise Settings")] 
    [Range(1,256)] [SerializeField] private int mapSize;
    [SerializeField] private int seed;

    [Header("Heightmap Noise Settings")]
    [Range(1f, 100f)] [SerializeField] private float noiseScale;
    [Range(0, 16)] [SerializeField] private int octaves;
    [Range(0, 1)] [SerializeField] private float persistance;
    [Range(1, 5)] [SerializeField] private float lacunarity;
    [SerializeField] private bool makeRigid;
    [SerializeField] private Vector2 offsetOctave;
    [SerializeField] private Vector2 offset;


    [Header("Cave Noise Settings")]
    [Range(0, 100)] [SerializeField] private int randomFillPrecent;
    [Range(0, 20)] [SerializeField] private int smoothingSteps;
    [Range(1, 5)] [SerializeField] private int scalingFactor;

    [Header("Inspector Settings")]
    public bool autoUpdate = true;

    public void GenerateMap()
    {
        float [,] noiseMap;

        MapDisplay display = FindObjectOfType<MapDisplay>();

        switch (noiseType)
        {
            default:
            case (NoiseType.HEIGHTMAP):
                noiseMap = NoiseGenerator.GenerateNoiseMap(mapSize, seed, noiseScale, octaves, persistance, lacunarity, offsetOctave, offset, makeRigid);
                display.DrawNoiseMap(noiseMap);
                break;

            case (NoiseType.CAVEMAP):
                NoiseGenerator.InitializeCaveNoise(mapSize/scalingFactor, seed, false, randomFillPrecent, smoothingSteps);
                noiseMap = NoiseGenerator.RandomFillMap();

                if (scalingFactor > 1)
                {
                    float[,] newNoiseMap = new float[mapSize, mapSize];
                    
                    for(int x=0; x<mapSize; x++)
                    {
                        for (int z = 0; z < mapSize; z++)
                        {
                            newNoiseMap[x, z] = noiseMap[Mathf.FloorToInt(x / scalingFactor), Mathf.FloorToInt(z / scalingFactor)];
                        }
                    }

                    display.DrawNoiseMapCave(newNoiseMap);
                }
                else 
                {
                    display.DrawNoiseMapCave(noiseMap);
                }

                break;
            case (NoiseType.SIMPLEX_SLICE):
                noiseMap = NoiseGenerator.EvaluateOSN(mapSize, seed, noiseScale, octaves, persistance, lacunarity, offsetOctave, offset, makeRigid);
                display.DrawNoiseMap(noiseMap);
                break;
        }
    }

    private void OnValidate()
    {
        int rest = mapSize % scalingFactor;
        mapSize -= rest;
    }

    private enum NoiseType
    {
        HEIGHTMAP,
        CAVEMAP,
        SIMPLEX_SLICE
    }
}
