using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MapDisplay : MonoBehaviour
{
    public Renderer textureRenderer;

    Texture2D noiseTexture;
    public RawImage noiseDp;

    [Range (0,1)]
    [SerializeField] private float waterLevel = 0.24f;
    [SerializeField] private Gradient landGradient;
    [SerializeField] private Gradient WaterGradient;
    static Color lightBlue = new Color(0.37f, 0.72f, 0.88f);

    public void DrawNoiseMap(float[,] noiseMap)
    {
        int width = noiseMap.GetLength(0);
        int height = noiseMap.GetLength(1);

        noiseTexture = new Texture2D(width, height);

        Color[] colorMap = new Color[width * height];

        for(int z = 0; z < height; z++)
        {
            for (int x = 0; x < width; x++)
            {
                colorMap[z * width + x] = Color.Lerp(Color.black, Color.white, noiseMap[x, z]);
                if (colorMap[z * width + x].grayscale < waterLevel)
                {
                    //Make color brighter for underwater
                    colorMap[z * width + x].r += 0.2f;
                    colorMap[z * width + x].g += 0.2f;
                    colorMap[z * width + x].b += 0.2f;
                    colorMap[z * width + x] *= WaterGradient.Evaluate(colorMap[z * width + x].grayscale);
                }
                else
                {
                    colorMap[z * width + x] *= landGradient.Evaluate(colorMap[z * width + x].grayscale);
                }
            }
        }
        noiseTexture.SetPixels(colorMap);
        noiseTexture.Apply();
        noiseTexture.filterMode = FilterMode.Point;

        textureRenderer.sharedMaterial.mainTexture = noiseTexture;
        noiseDp.texture = noiseTexture;
        textureRenderer.transform.localScale = new Vector3(width, 1, height);
    }

    public void DrawNoiseMapCave(float[,] noiseMap)
    {
        int width = noiseMap.GetLength(0);
        int height = noiseMap.GetLength(1);

        noiseTexture = new Texture2D(width, height);


        Color[] colorMap = new Color[width * height];

        for (int z = 0; z < height; z++)
        {
            for (int x = 0; x < width; x++)
            {
                colorMap[z * width + x] = Color.Lerp(Color.white, Color.black, noiseMap[x, z]);
            }
        }
        noiseTexture.SetPixels(colorMap);
        noiseTexture.Apply();
        noiseTexture.filterMode = FilterMode.Point;
        //noiseTexture.anisoLevel = 0;

        //noiseTexture.Compress(true);

        textureRenderer.sharedMaterial.mainTexture = noiseTexture;
        textureRenderer.transform.localScale = new Vector3(width, 1, height);
    }

}
