using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chunk
{

    bool smoothTerrain { get { return GameData.SmoothTerrain; } }
    bool shareVertices { get { return GameData.ShareVertices; } }

    public bool meshIsGenerated = false;
    public bool waterIsAdded = false;

    List<Vector3> vertices = new List<Vector3>();
    List<int> triangles = new List<int>();

    public GameObject chunkObject;

    public float minHeight = float.MaxValue, maxHeight = float.MinValue;
    MeshFilter meshFilter;
    MeshCollider meshCollider;
    MeshRenderer meshRenderer;
    private Mesh mesh;

    public Vector3Int chunkPosition;
    float[,,] terrainDensityMap;
    float[,] terrainHeightMap;


    int chunkWidth { get { return GameData.ChunkWidth; } }
    int chunkHeight { get { return GameData.ChunkHeight; } }
    float surfaceDensity { get { return GameData.SurfaceDensity; } }

    public string terrainGenProgress = "";

    public Chunk(Vector3Int _position)
    {
        chunkPosition = _position;

        chunkObject = new GameObject();
        chunkObject.name = string.Format("Chunk_X{0}Z{1}", _position.x, _position.z);
        chunkObject.transform.position = _position;

        meshFilter = chunkObject.AddComponent<MeshFilter>();
        meshCollider = chunkObject.AddComponent<MeshCollider>();
        meshRenderer = chunkObject.AddComponent<MeshRenderer>();
        meshRenderer.material = GameData.terrainMat;

        //chunkObject.transform.tag = "Terrain";
        chunkObject.layer = 6; //terrain layer

        terrainDensityMap = new float[chunkWidth + 1, chunkHeight + 1, chunkWidth + 1];
        terrainHeightMap = new float[chunkWidth + 1, chunkWidth + 1];
        mesh = new Mesh();
    }

    public void PopulateTerrainDensityMap()
    {
        minHeight = float.MaxValue;
        maxHeight = float.MinValue;

        for (int x = 0; x < chunkWidth + 1; x++)
        {
            for (int y = 0; y < chunkHeight + 1; y++)
            {
                for (int z = 0; z < chunkWidth + 1; z++)
                {
                    if (y == 0)
                    {
                        //Get Terrain Height using layers of regular PerlinNoise
                        terrainHeightMap[x, z] = GameData.GenerateTerrainHeightNoise(x + chunkPosition.x, z + chunkPosition.z);

                        //Get max and min value of terrain, caves aren't taken into account
                        if (terrainHeightMap[x, z] > maxHeight)
                        {
                            maxHeight = terrainHeightMap[x, z];
                        }
                        if (terrainHeightMap[x, z] < minHeight)
                        {
                            minHeight = terrainHeightMap[x, z];
                        }
                    }

                    //float normalizedDensity = (float)Mathf.Max(Mathf.Min(1f, y - terrainHeightMap[x, y, z]), -1f);
                    float normalizedDensity = (float)Mathf.Clamp(y - terrainHeightMap[x, z], -1f, 1f);

                    if (GameData.GenerateCaves)
                    {
                        normalizedDensity = GameData.GenerateCaveSystem(
                            x + chunkPosition.x,
                            y + chunkPosition.y,
                            z + chunkPosition.z,
                            normalizedDensity,
                            terrainHeightMap[x, z]);
                    }


                    //populating terrain mesh with reverse height values
                    terrainDensityMap[x, y, z] = normalizedDensity;

                    //Optimized way to create mesh Data without extra iterration
                    //We want first to populate terrain and then MarchCubes on generated part of the terrain
                    //That's why MarchCube algorithm is called one step later. Usually it requires extra for-loop itteration, look CreateMeshData() function
                    if (x > 0 && y > 0 && z > 0)
                        MarchCube(new Vector3Int(x - 1, y - 1, z - 1));

                }
            }
        }
    }

    public void CreateMeshData()
    {
        ClearMeshData();
        MarchCubes();
        BuildMesh();
    }

    int GetCubeConfiguration(float[] cube)
    {
        int configIndex = 0;
        for (int i = 0; i < 8; i++)
        {
            if (cube[i] > surfaceDensity)
                configIndex |= 1 << i;
        }

        return configIndex;
    }

    public void MarchCubes()
    {
        for (int x = 0; x < chunkWidth; x++)
        {
            for (int y = 0; y < chunkHeight; y++)
            {
                for (int z = 0; z < chunkWidth; z++)
                {
                    MarchCube(new Vector3Int(x, y, z));
                }
            }
        }
    }

    void MarchCube(Vector3Int position)
    {
        // Sample terrain values at each corner of the cube.
        float[] cube = new float[8];
        for (int i = 0; i < 8; i++)
        {
            cube[i] = SampleTerrainDensityAtCorner(position + GameData.CornerTable[i]);
        }

        // Get the configuration index of this cube.
        int configIndex = GetCubeConfiguration(cube);

        // If the configuration of the cube is 0 or 255 (hidden solid or air) we don't do anything.
        if (configIndex == 0 || configIndex == 255)
            return;

        // Loop trough triangles. There are never more than 5 triangles to a cube, 3 vertices to a triangle.
        int edgeIndex = 0;
        for (int triangle = 0; triangle < 5; triangle++)
        {
            for (int point = 0; point < 3; point++)
            {
                // Get the current indice. We increment triangleIndex each loop.
                int indice = GameData.TriangleTable[configIndex, edgeIndex];

                // If the current edge is -1, there are no more indices and we can exit the function.
                if (indice == -1)
                    return;

                // Get the vertices for the start and end of this edge.
                Vector3 vert1 = position + GameData.CornerTable[GameData.EdgeIndexes[indice, 0]];
                Vector3 vert2 = position + GameData.CornerTable[GameData.EdgeIndexes[indice, 1]];

                Vector3 vertPosition = Vector3.zero;
                if (smoothTerrain)
                {
                    // Get the terrain values at either end of our current edge from the cube array created above.
                    float vert1Sample = cube[GameData.EdgeIndexes[indice, 0]];
                    float vert2Sample = cube[GameData.EdgeIndexes[indice, 1]];

                    // Calculate the difference between the terrain values.
                    float difference = vert2Sample - vert1Sample;

                    // If the difference is 0, then the terrain is passes through the middle.
                    if (difference == 0)
                        difference = surfaceDensity;
                    else
                        difference = (surfaceDensity - vert1Sample) / difference;

                    // Calculate the point along the edge that the terrain passes trough.
                    vertPosition = vert1 + ((vert2 - vert1) * difference);
                }
                else
                {
                    // Get the midpoint of this edge.
                    vertPosition = (vert1 + vert2) / 2f;
                }

                // Add to our vertices and triangles list and increment the edgeIndex.               
                if (shareVertices)
                {
                    triangles.Add(VertForIndice(vertPosition));
                }
                else
                {
                    vertices.Add(vertPosition);
                    triangles.Add(vertices.Count - 1);
                }

                edgeIndex++;
            }
        }

    }

    public void FlattenTerrain(Vector3 pos, float radius)
    {
        // Height of the aimed point
        float voxelFloorHeight = Mathf.Floor(pos.y);    
        // height from voxel bottom to the point on the surface
        float voxelHeight = pos.y - voxelFloorHeight;
        // 1% of current height, used to calculate density of top point
        float densityModifier = 1 / (voxelHeight * 100); //one precent

        Vector3Int v3Int = new Vector3Int(Mathf.CeilToInt(pos.x), Mathf.CeilToInt(pos.y), Mathf.CeilToInt(pos.z));
        v3Int -= chunkPosition;

        for (int x = Mathf.Max(v3Int.x - (int)radius, 0); x < Mathf.Min(v3Int.x + (int)radius, chunkWidth + 1); x++)
            for (int y = Mathf.Max(v3Int.y - (int) radius, 1); y < Mathf.Min(v3Int.y, chunkHeight); y++)
                for (int z = Mathf.Max(v3Int.z - (int)radius, 0); z < Mathf.Min(v3Int.z + (int)radius, chunkWidth + 1); z++)
                {
                    // set density of any point based on aimed height
                    // clamp result between -1 and 1 (keep organized)
                    float density = Mathf.Clamp(densityModifier * (y - voxelFloorHeight) - 1, -1, 1);
                    terrainDensityMap[x, y, z] = density;
                }

        CreateMeshData();
    }

    public void TerraformTerrain(Vector3 pos, float radius, TerraformingDeformationType deformationType)
    {
        Vector3Int v3Int = new Vector3Int(Mathf.CeilToInt(pos.x), Mathf.CeilToInt(pos.y), Mathf.CeilToInt(pos.z));
        v3Int -= chunkPosition;

        for (int x = Mathf.Max(v3Int.x - (int)(radius + 1), 0); x < Mathf.Min(v3Int.x + (int)(radius + 1), chunkWidth + 1); x++)
            for (int y = Mathf.Max(v3Int.y - (int)(radius + 1), 1); y < Mathf.Min(v3Int.y + (int)(radius + 1), chunkHeight); y++)
                for (int z = Mathf.Max(v3Int.z - (int)(radius + 1), 0); z < Mathf.Min(v3Int.z + (int)(radius + 1), chunkWidth + 1); z++)
                {

                    float dist = Vector3.Distance(v3Int, new Vector3(x, y, z));
                    if (deformationType == TerraformingDeformationType.Adding)
                    {
                        float dens = -radius + dist;
                        terrainDensityMap[x, y, z] = Mathf.Min(terrainDensityMap[x, y, z], dens);
                    }
                    else if (deformationType == TerraformingDeformationType.Removing)
                    {
                        float dens = radius - dist;
                        terrainDensityMap[x, y, z] = Mathf.Max(terrainDensityMap[x, y, z], dens);
                    }
                }

        CreateMeshData();
    }

    public Vector3Int PrintPointValue(Vector3 pos)
    {
        Vector3Int globalV3Int = new Vector3Int(Mathf.CeilToInt(pos.x), Mathf.CeilToInt(pos.y), Mathf.CeilToInt(pos.z));
        Vector3Int v3Int = globalV3Int - chunkPosition;
        Debug.Log("Terrain_On_X" + v3Int.x + "_Y" + v3Int.y + "_Z" + v3Int.z + " has value:" + terrainDensityMap[v3Int.x, v3Int.y, v3Int.z]);
        return globalV3Int;
    }

    //Get the terrain value storred at given point of the terrainMap.
    public float SampleTerrainDensityAtCorner(Vector3Int point)
    {
        return terrainDensityMap[point.x, point.y, point.z];
    }

    int VertForIndice(Vector3 vert)
    {
        // Loop through all the vertices currently in the vertices list.
        for (int i = 0; i < vertices.Count; i++)
        {

            // If we find a vert that matches ours, then simply return this index.
            if (vertices[i] == vert)
                return i;

        }

        // If we didn't find a match, add this vert to the list and return last index.
        vertices.Add(vert);
        return vertices.Count - 1;
    }

    //Use it before you repopulate mesh data
    public void ClearMeshData()
    {
        meshCollider.sharedMesh.Clear();
        meshFilter.mesh.Clear();
        mesh.Clear();
        triangles.Clear();
        vertices.Clear();
    }

    public void BuildMesh()
    {
        //mesh = new Mesh();
        mesh.vertices = vertices.ToArray();
        mesh.triangles = triangles.ToArray();

        //Vector2[] uv = (mesh.uv.Length == vertices.Count) ? mesh.uv : new Vector2[vertices.Count];
        
        Vector2[] uvs1 = new Vector2[vertices.Count];
        Vector2[] uvs2 = new Vector2[vertices.Count];
        Vector2[] uvs3 = new Vector2[vertices.Count];

        for (int i = 0; i < uvs1.Length; i++)
        {
            uvs1[i] = new Vector2(vertices[i].x, vertices[i].z);
            uvs2[i] = new Vector2(vertices[i].x, vertices[i].y);
            uvs3[i] = new Vector2(vertices[i].y, vertices[i].z);
        }
        mesh.uv = uvs1;
        mesh.uv2 = uvs2;
        mesh.uv3 = uvs3;

        //mesh.RecalculateBounds();
        //mesh.RecalculateTangents();
        mesh.RecalculateNormals();
        mesh.RecalculateUVDistributionMetrics();
        meshFilter.mesh = mesh;
        meshCollider.sharedMesh = mesh;
        
        if(!waterIsAdded)
            chunkObject.transform.parent.transform.parent.GetComponent<WorldGenerator>().GenerateWater(this);
    }
}
