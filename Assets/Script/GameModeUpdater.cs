using TMPro;
using UnityEngine;

public class GameModeUpdater : MonoBehaviour
{
    [SerializeField] private TMP_Text gameModeLabel;
    [SerializeField] private TMP_Text genTimeLabel;

    // Update is called once per frame
    void Update()
    {
        updateGameModeLabel();
        genTimeLabel.SetText("Generation time: "+ GameData.generationTime + " seconds");
    }

    private void updateGameModeLabel()
    {
        switch (GameData.gameMode)
        {
            case GameMode.FreeCam:
                gameModeLabel.SetText("Free Cam");
                break;
            case GameMode.Editor:
                gameModeLabel.SetText("Editor");
                break;
            case GameMode.Terraforming:
                gameModeLabel.SetText("Terraforming");
                break;
            default:
            case GameMode.Undifined:
                gameModeLabel.SetText("Undifined");
                break;
        }
    }
}
