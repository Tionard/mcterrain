using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.Diagnostics;
using System.Threading;
public class WorldGenerator : MonoBehaviour
{

    private int worldSizeInChunks { get { return GameData.WorldSizeInChunks; } }
    
    [Header("World Options")]
    [SerializeField] private bool generateWater;
    [SerializeField] private bool generateCaves;

    [Header("Multy Threading")]
    [Range(1,10)]
    [SerializeField] private int MaxNumberOfThreads = 4;

    [Header("Required Objects")]
    [SerializeField] private GameObject waterPrefab;

    [Header("Misc. Connections")]
    [SerializeField] private GameObject loadingScreen;
    [SerializeField] private TMP_Text loadingInfo;

    Dictionary<Vector3Int, Chunk> chunks = new Dictionary<Vector3Int, Chunk>();
    public List<Vector3Int> chunksInQueue { get; private set; }
    Dictionary<Vector3Int, GameObject> waterChunks = new Dictionary<Vector3Int, GameObject>();

    List<Thread> threads = new List<Thread>();

    [SerializeField] private GameObject LandGroup, WaterGroup;

    private float timeLeft = 0, timeBuf = 0;
    private long stampTimeBuf;
    public bool firstGenComplete;

    private PerformanceCounter cpuCounter;
    private PerformanceCounter ramCounter;

    public string getCurrentCpuUsage()
    {
        return cpuCounter.NextValue() + "%";
    }

    public string getAvailableRAM()
    {
        return ramCounter.NextValue() + "MB";
    }

    // Start is called before the first frame update
    private void Awake()
    {
        GameData.seed = Random.Range(-10000, 10000);
    }

    void Start()
    {
        GameData.gameMode = GameMode.FreeCam;
        chunksInQueue = new List<Vector3Int>();       
        cpuCounter = new PerformanceCounter("Processor", "% Processor Time", "_Total");
        ramCounter = new PerformanceCounter("Memory", "Available MBytes");
    }

    private void Update()
    {   
        if (chunksInQueue.Count >= 1)
        {
            ConstructMesh();
        }
    }

    private void ConstructMesh()
    {
        foreach (Vector3Int pos in chunksInQueue)
        {
            // if no threads are free, and pool is full, returns -1
            int tIndex = ReturnAvailableThreadPointer();
            if (tIndex != -1)
            {   // if thread is idle, assign new task to it
                threads[tIndex] = new Thread(chunks[pos].PopulateTerrainDensityMap);
            }
            else if (threads.Count <= MaxNumberOfThreads)
            {   // if pool of threads isn't fool, add new thread
                // and assign new task
                threads.Add(new Thread(chunks[pos].PopulateTerrainDensityMap));
                tIndex = threads.Count - 1; //return index of the thread
            }

            if (tIndex != -1 || threads.Count <= MaxNumberOfThreads)
            {   // Start the thread and coroutine, remove key from list
                threads[tIndex].Start();
                StartCoroutine(BuildChunkMesh(pos,threads[tIndex]));
                chunksInQueue.Remove(pos);
                break;
            }
        }
    }
    
    private int ReturnAvailableThreadPointer()
    {
        for (int i = 0; i < threads.Count; i++)
        {
            if (threads[i] == null || !threads[i].IsAlive)
            {
                return i;
            }
        }
        return -1;
    }

    private IEnumerator BuildChunkMesh(Vector3Int position, Thread thread)
    {
        while (thread.IsAlive)
        {
            yield return null;
        }
        chunks[position].BuildMesh();

        if (chunksInQueue.Count < 1)
        {
            GameData.generationTime = Time.time - timeBuf;
            UnityEngine.Debug.Log(GameData.generationTime + " seconds");
        }
    }

    public void ReconstructMesh()
    {
        if (!firstGenComplete)
        {
            timeBuf = Time.time;
            InitChunksForQueue();
            firstGenComplete = true;
        }
        else
        {
            timeBuf = Time.time;
            foreach (GameObject g in waterChunks.Values)
            {
                Destroy(g);
            }
            waterChunks.Clear();
            foreach (Chunk chunk in chunks.Values)
            {
                chunk.waterIsAdded = false;
                chunksInQueue.Add(chunk.chunkPosition);
                chunk.ClearMeshData();
            }
        }
    } 

    void InitChunksForQueue()
    {
        for (int x = 0; x < worldSizeInChunks; x++)
        {
            for (int z = 0; z < worldSizeInChunks; z++)
            {
                Vector3Int chunkPos = new Vector3Int(x * GameData.ChunkWidth, 0, z * GameData.ChunkWidth);
                Chunk chunk = new Chunk(chunkPos);
                chunks.Add(chunkPos, chunk);
                chunks[chunkPos].chunkObject.transform.SetParent(LandGroup.transform);
                chunksInQueue.Add(chunkPos);
                GenerateWater(chunk);
            }
        }
    }

    public void GenerateWater(Chunk chunk)
    {
        //if (chunk.minHeight <= GameData.BaseWaterLevel)
        if (GetGenerateWater()) // if water generation is enabled
        {
            Vector3Int chunkPos = chunk.chunkPosition;  // assign position
            GameObject aWater = Instantiate(waterPrefab, new Vector3(chunkPos.x, GameData.BaseWaterLevel, chunkPos.z), Quaternion.identity);
            aWater.name = string.Format("WaterX{0}Z{1}", chunkPos.x, chunkPos.z); 
            aWater.transform.SetParent(WaterGroup.transform); // assign it to group.
            waterChunks.Add(chunkPos, aWater);  // add it to dictionary.
            chunk.waterIsAdded = true;          // 
        }
    }

    private IEnumerator GenerateChunks()
    {
        while (chunks.Count < worldSizeInChunks * worldSizeInChunks)
        {
            for(int x = 0; x < worldSizeInChunks; x++)
            {
                for (int z = 0; z < worldSizeInChunks; z++)
                {
                    timeBuf = (float)(cpuCounter.NextSample().TimeStamp100nSec - stampTimeBuf) / 10000000;
                    stampTimeBuf = cpuCounter.NextSample().TimeStamp100nSec;
                    timeLeft = timeBuf * (worldSizeInChunks * worldSizeInChunks - (x * worldSizeInChunks + z + 1));
                    //UnityEngine.Debug.Log(timeLeft);

                    string info = string.Format("Generating chunk #{0} out of {1}... \nEstimated time left: {2}seconds", x * worldSizeInChunks + z + 1, worldSizeInChunks * worldSizeInChunks, (int)timeLeft);
                    loadingInfo.SetText(info);

                    Vector3Int chunkPos = new Vector3Int(x * GameData.ChunkWidth, 0, z * GameData.ChunkWidth);
                    Chunk chunk = new Chunk(chunkPos);
                    chunks.Add(chunkPos, chunk);
                    chunks[chunkPos].chunkObject.transform.SetParent(LandGroup.transform);
                    GenerateWater(chunk);

                    yield return null;
                }

            }
            UnityEngine.Debug.Log(string.Format("world of {0} x {0} chunks is generated", worldSizeInChunks));
            loadingScreen.SetActive(false);
        }
    }

    public Chunk GetChunkFromVector3(Vector3 pos)
    {
        int x = (int)pos.x;
        int z = (int)pos.z;

        Vector3Int chunkPos = ConvertCoordsToChunkCoord(x, z);

        UnityEngine.Debug.Log(chunkPos);
        return chunks[chunkPos];

    }

    public Chunk[] GetChunksFromVector3(Vector3 point, float radius)
    {
        int rad = Mathf.RoundToInt(Mathf.Abs(radius));
        List<Chunk> _chunks = new List<Chunk>();
        
        for (int x = (int)point.x - rad; x <= (int)point.x + rad; x++)
        {
            for (int z = (int)point.z - rad; z <= (int) point.z + rad; z++)
            {
                Vector3Int chunkPos = ConvertCoordsToChunkCoord(x, z);

                if(chunks.ContainsKey(chunkPos) && !_chunks.Contains(chunks[chunkPos]))
                {
                    _chunks.Add(chunks[chunkPos]);
                }
            }
        }
        UnityEngine.Debug.Log("chunks in terra-tool radius:" + _chunks.Count);
        return _chunks.ToArray();

    }

    public void ModifyTerrain(Vector3 pos, float radius, TerraformingDeformationType deformType)
    {
        Chunk[] _chunks = GetChunksFromVector3(pos, radius + 1);
        foreach (Chunk chunk in _chunks)
        {
            if(deformType == TerraformingDeformationType.Flattening)
            {
                chunk.FlattenTerrain(pos, radius);
            }
            else
            {
                chunk.TerraformTerrain(pos, radius, deformType);
            }

        }
    }

    private Vector3Int ConvertV3ToChunkCoord(Vector3 position)
    {
        int _x = GameData.ChunkWidth * Mathf.FloorToInt(position.x / GameData.ChunkWidth);
        int _z = GameData.ChunkWidth * Mathf.FloorToInt(position.z / GameData.ChunkWidth);
        return new Vector3Int(_x, 0, _z);
    }

    private Vector3Int ConvertCoordsToChunkCoord(int x, int z)
    {
        int _x = GameData.ChunkWidth * Mathf.FloorToInt(x / GameData.ChunkWidth);
        int _z = GameData.ChunkWidth * Mathf.FloorToInt(z / GameData.ChunkWidth);
        return new Vector3Int(_x, 0, _z);
    }

    private bool GetGenerateWater()
    {
        generateWater = GameData.GenerateWater;
        return generateWater;
    }

    private bool GetGenerateCaves()
    {
        generateCaves = GameData.GenerateCaves;
        return generateCaves;
    }

    private void SetGenerateWater()
    {
        GameData.GenerateWater = generateWater;
    }
    private void SetGenerateCaves()
    {
        GameData.GenerateCaves = generateCaves;
    }


    private void OnValidate()
    {
        SetGenerateWater();
        SetGenerateCaves();
    }
}
