This project allows to generate terrain of n*n chunks. 
Each chunk is consists of up to 16x128x16(x,y,z) "cubes" but usually much less.

The main object in the scene is the worldGenerator. 
In inspector it has few parameters:

World Size In Chunks - defines how big the world would be (n*n).
Generate Water, Generate Caves - this are self-explanatory. Need to be set/unset before generation

Number Of Threads - each thread will generate data for a separate chunk in parallel with other threads. 
For smooth experience don't set it heigher then amoung of you logic cores on processor. Higher threads cound will produce faster result but also fps can drop until the generation is finished.

Camera got 2 extra controls for Terraforming:
Radius - effective radius
Speed - how often/second the method is called

Gen.Mode -> opens UI that allows to change parameters and generate new terrain. 
It's innactive while terrain is generating for data-safety reasons.
